#include "stdafx.h"
#include "HeldKarpSolver.h"
#include <algorithm>
#include <unordered_set>
#include "CalculatedSolution.h"
#include <iostream>

HeldKarpSolver::HeldKarpSolver() {}


HeldKarpSolver::~HeldKarpSolver() {
	Solutions.clear();
}

void HeldKarpSolver::Clear() {
	Solutions.clear();
}

Model HeldKarpSolver::Solve(Model matrix) {
	Model* operational = new Model(matrix);
	for (int i = 0; i < 1; i++) {

		auto dest = new int[operational->NumberOfCities - 1];
		int index = 0;
		for (int j = 0; j < operational->NumberOfCities; j++)
			if (i != j)
				dest[index++] = (operational->Columns->at(j));
		auto a = GetMinimumCostRoute(*operational, 0, dest, operational->NumberOfCities - 1);
		matrix.Result = a.totalCost;
		matrix.Path = a.Path;
	}
	operational->Result = matrix.Result;
	operational->Path = matrix.Path;
	return *operational;
}

TModel HeldKarpSolver::GetMinimumCostRoute(Model& data, int fromCity, int* destCities, int numerOfCitiesToVisit) {
	TModel result;
	if (numerOfCitiesToVisit == 0) {
		result.totalCost = data.Matrix[fromCity][0];
		auto point = new Point(fromCity, 0);
		result.Path.push_back(point);
		return result;
	}

	result.totalCost = std::numeric_limits<double>::max();
	int selectedIndex = 0;

	for (int j = 0; j < numerOfCitiesToVisit; j++) {

		double costOfVisitingCurrentCity = data.Matrix[fromCity][destCities[j]];
		int* newDestCities;
		if (numerOfCitiesToVisit > 1) {
			int index = 0;
			newDestCities = new int[numerOfCitiesToVisit - 1];
			for (int k = 0; k < numerOfCitiesToVisit; k++) {
				if (k != j) {
					newDestCities[index++] = destCities[k];
				}
			}
		} else {
			newDestCities = new int[1];
		}
		TModel temp;
		CalculatedSolution* s = new CalculatedSolution(destCities[j], newDestCities, numerOfCitiesToVisit - 1);
		std::vector<CalculatedSolution>::iterator found = find(Solutions.begin(), Solutions.end(), *s);
		if (found != Solutions.end()) {
			temp = (*found).Result;
		} else {
			temp = GetMinimumCostRoute(data, destCities[j], newDestCities, numerOfCitiesToVisit - 1);
			auto solution = new CalculatedSolution(destCities[j], newDestCities, numerOfCitiesToVisit - 1);
			solution->Result = temp;
			Solutions.push_back(*solution);
		}


		delete[] newDestCities;
		double costOfVisitingOtherCities = temp.totalCost;

		double currentCost = costOfVisitingCurrentCity + costOfVisitingOtherCities;
		if (result.totalCost > currentCost) {
			result.totalCost = currentCost;
			result.Path = temp.Path;
			auto point = new Point(fromCity, destCities[j]);
			result.Path.push_back(point);
		}
	}
	return result;
}
