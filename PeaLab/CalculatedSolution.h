#pragma once
#include "CalculatedSolution.h"
#include "TModel.h"

class CalculatedSolution {
public:
	int FromCity;
	std::vector<int>* ToCities;
	TModel Result;
	bool operator ==(const CalculatedSolution& b) const;
	CalculatedSolution();
	CalculatedSolution(int fromCity, int* toCities, int length);
	~CalculatedSolution();
};
