#pragma once
#include "Model.h"

class MatrixGenerator {
public:
	MatrixGenerator();
	~MatrixGenerator();
	Model* Generate(int size);
};
