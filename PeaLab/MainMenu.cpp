#include "stdafx.h"
#include "MainMenu.h"
#include <iostream>
#include <stdlib.h>
#include "FileReader.h"
#include "Model.h"
#include "ISolver.h"
#include "BranchAndBoundSolver.h"
#include <memory>
#include "Timer.h"
#include "MatrixGenerator.h"
#include "Statistics.h"
#include "IWriter.h"
#include "CsvWriter.h"
#include <iomanip>
#include <iostream>
#include "HeldKarpSolver.h"
#include "ApproximateSolver.h"
using namespace std;


enum MenuOptions {
	Read = 1,
	Print = 2,
	SolveBB = 3,
	SolveDynamic = 4,
	SolveApproximate = 5,
	TestBenchBB = 6,
	TestBenchDynamic = 7,
	TestApproximate = 8,
	Quit = 9
};

MainMenu::MainMenu() {
	data = new Model();
}


MainMenu::~MainMenu() {
	delete data;
}

void MainMenu::Draw() {
	while (true) {
		try {
			int choiceIn;
			MenuOptions choice;
			cout << "MAIN MENU\n";
			cout << "1. Czytaj z pliku\n";
			cout << "2. Pokaz dane\n";
			cout << "3. Branch & Bound\n";
			cout << "4. Programowanie dynamiczne\n";
			cout << "6. Test - Branch & Bound\n";
			cout << "7. Test - Programowanie dynamiczne\n";
			cout << "9. Wyjscie\n";
			cin >> choiceIn;
			choice = static_cast<MenuOptions>(choiceIn);
			ISolver* solver = nullptr;
			switch (choice) {
				case Read:
					ReadAction();
					break;
				case Print:
					PrintAction();
					break;
				case SolveBB:
					solver = new BranchAndBoundSolver();
					SolveAction(solver);
					delete solver;
					break;
				case SolveDynamic:
					solver = new HeldKarpSolver();
					SolveAction(solver);
					delete solver;
					break;
				case SolveApproximate:
					solver = new ApproximateSolver();
					SolveAction(solver);
					delete solver;
					break;
				case TestBenchBB:
					solver = new BranchAndBoundSolver();
					TestBenchAction(solver);
					delete solver;
					break;
				case TestBenchDynamic:
					solver = new HeldKarpSolver();
					TestBenchAction(solver);
					delete solver;
					break;
				case TestApproximate:
					solver = new ApproximateSolver();
					TestBenchAction(solver);
					delete solver;
					break;
				case Quit:
					exit(EXIT_SUCCESS);
				default: break;
			}
		} catch (exception) {
			cout << "Nieprawidlowa operacja. Sprobuj ponownie.";
			getchar();
		}
	}
}

void MainMenu::PrintAction() {
	data->ToString();
}

void MainMenu::ReadAction() {
	string filename;
	cout << "Podaj nazwe pliku: ";
	cin >> filename;
	FileReader reader;
	data = reader.Read(filename);
}

void MainMenu::SolveAction(ISolver* solver) {
	Model solution = solver->Solve(*data);

	cout << "Koszt: " << solution.Result << "\nDroga: ";
	solution.PrintPath();
	cout << endl;
}

void MainMenu::TestBenchAction(ISolver* solver) {
	vector<Statistics>* stats = new vector<Statistics>();
	int numberOfTests = 100;
	MatrixGenerator* generator = new MatrixGenerator();
	Timer* timer = new Timer();
	for (int i = 2; i <= 12; i += 2) {
		double totalTime = 0;
		for (int testNo = 0; testNo < numberOfTests; testNo++) {
			auto data = *generator->Generate(i);
			timer->Reset();
			timer->Start();
			solver->Solve(data);
			solver->Clear();
			timer->Stop();
			totalTime += timer->GetElapsedTime();
			cout << i << " Cities: "
					<< testNo << " of " << numberOfTests
					<< "(" << timer->GetElapsedTime() << ")" << endl;
		}
		double averageTime = totalTime / numberOfTests;
		stats->push_back(*new Statistics(i, averageTime));
	}

	IWriter* writer = new CsvWriter();
	writer->Write("report.csv", *stats);
}
