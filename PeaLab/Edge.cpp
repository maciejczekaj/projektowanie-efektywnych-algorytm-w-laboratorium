#include "stdafx.h"
#include "Edge.h"

Edge::Edge(Edge* edge) {
	Point1 = edge->Point1;
	Point2 = edge->Point2;
	Cost = edge->Cost;
	Visited = false;
}

Edge::Edge(int point1, int point2, unsigned cost) {
	Point1 = point1;
	Point2 = point2;
	Cost = cost;
	Visited = false;
}

Edge::~Edge() {}

void Edge::Visit() {
	Visited = true;
}