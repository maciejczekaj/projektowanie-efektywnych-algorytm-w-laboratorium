#pragma once
#include "ISolver.h"
#include "Model.h"


class BranchAndBoundSolver :
		public ISolver {
public:
	BranchAndBoundSolver();
	~BranchAndBoundSolver();
private:
	struct CriticalPoint {
		unsigned x, y;
		unsigned rank;
	};

	const int self = -1;

	unsigned lowerLimit = 0;

	void Scale(Model& matrix);
	void ScaleRows(Model& matrix);
	void ScaleColumns(Model& matrix);

	void IncreaseLowerLimit(unsigned int value);
	void PrintLowerLimit() const;

	void RemoveMostCriticalPoint(CriticalPoint point, Model& matrix);
	void BlockBackpaths(Model& matrix, Point* point);
	Model Solve(Model matrix, bool goRight);
	void PrintPath(Model& matrix);
	CriticalPoint FindMostCriticalPoint(Model& matrix);
	// Inherited via ISolver
	virtual Model Solve(Model matrix) override;
	virtual void Clear() override;
};
