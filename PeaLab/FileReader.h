#pragma once
#include "Model.h"
#include <string>

class FileReader {
public:
	FileReader();
	~FileReader();
	static Model* Read(std::string filename);
};
