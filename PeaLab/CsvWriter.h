#pragma once
#include "IWriter.h"

class CsvWriter :
		public IWriter {
public:
	CsvWriter();
	~CsvWriter();
protected:
	void Write(std::string filename, std::vector<Statistics> stats) override;
};
