#include "stdafx.h"
#include "MatrixGenerator.h"
#include <ctime>


MatrixGenerator::MatrixGenerator() {
	srand(time(nullptr));
}


MatrixGenerator::~MatrixGenerator() {}

Model* MatrixGenerator::Generate(int size) {
	auto data = new Model();
	data->NumberOfCities = size;
	data->Matrix = new int*[size];
	data->Rows = new std::vector<unsigned>(size);
	data->Columns = new std::vector<unsigned>(size);
	for (auto i = 0; i < size; i++) {
		data->Matrix[i] = new int[size];
		for (auto j = 0; j < size; j++) {
			data->Matrix[i][j] = rand();
		}
		data->Rows->at(i) = i;
		data->Columns->at(i) = i;
	}
	return data;
}
