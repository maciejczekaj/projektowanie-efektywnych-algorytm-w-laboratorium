#include "stdafx.h"
#include "Model.h"
#include <iostream>
#include <iomanip>

using namespace std;


Model::Model() {
	Result = numeric_limits<unsigned>::max();
}

Model::Model(const Model& copy) {
	NumberOfCities = copy.NumberOfCities;
	Result = copy.Result;
	Path = copy.Path;
	if (copy.Columns != nullptr && copy.Rows != nullptr && NumberOfCities != 0) {
		try {
			Columns = new vector<unsigned>(NumberOfCities);
			Rows = new vector<unsigned>(NumberOfCities);
			std::copy(copy.Columns->begin(), copy.Columns->end(), Columns->begin());
			std::copy(copy.Rows->begin(), copy.Rows->end(), Rows->begin());
		} catch (exception e) {}
	}

	if (NumberOfCities == 0) Matrix = new int*[1];
	else {
		//Matrix = copy.Matrix;
		Matrix = new int*[NumberOfCities];
		for (int i = 0; i < NumberOfCities; i++) {
			Matrix[i] = new int[NumberOfCities];
			for (int j = 0; j < NumberOfCities; j++) {
				Matrix[i][j] = copy.Matrix[i][j];
			}
		}
	}
}

Model::~Model() {
	//for(int i = 0; i < NumberOfCities; i++)
	//	delete[] Matrix[i];
	//delete[] Matrix;
	//delete Rows;
	//delete Columns;
}

void Model::ToString() {
	cout << "\t";
	//cout << "Liczba miast:\t" << this->NumberOfCities << "\n\nMacierz:\n";
	for (auto i = this->Columns->begin(); i != this->Columns->end(); i++)
		cout << setw(2) << *i << "\t";
	cout << endl;
	for (auto i = this->Rows->begin(); i != this->Rows->end(); i++) {
		cout << setw(2) << *i << "\t";
		for (auto j = this->Columns->begin(); j != this->Columns->end(); j++) {
			cout << this->Matrix[*i][*j] << "\t";
		}
		cout << endl;
	}
}

int Model::Find(unsigned value, bool place) // place = 0 - row, 1-column
{
	for (auto it = Path.begin(); it != Path.end(); it++) {
		if (place && (*it)->Column == value)
			return (*it)->Row;
		if (!place && (*it)->Row == value)
			return (*it)->Column;
	}
	return -1;
}

void Model::PrintPath() {
	vector<unsigned>* way = new vector<unsigned>();

	int* points = new int[NumberOfCities];
	for (int i = 0; i < NumberOfCities; i++) points[i] = 0;
	for (auto it = Path.begin(); it != Path.end(); it++) {
		points[(*it)->Column]++;
		points[(*it)->Row]++;
	}
	int start = 0, current = 0, next;
	for (int i = 0; i < NumberOfCities; i++) {
		if (points[i] == 1) {
			start = i;
			current = i;
			break;
		}
	}
	cout << start << "->";
	for (int i = 0; i < NumberOfCities - 1; i++) {
		next = Find(current, 0);
		if (next >= 0) {
			current = next;
			cout << current << "->";
		}
	}
	cout << start << endl;
}
