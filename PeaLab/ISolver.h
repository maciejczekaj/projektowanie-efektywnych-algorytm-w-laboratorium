#pragma once
#include "Model.h"
#include <list>
#include "Point.h"

class ISolver {
public:
	Model* OriginalMatrix;

	virtual ~ISolver() {};
	virtual Model Solve(Model matrix) = 0;
	virtual void Clear() = 0;
};
