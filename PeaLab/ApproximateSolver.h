#pragma once
#include "ISolver.h"
#include "Edge.h"
#include <list>

class ApproximateSolver :
		public ISolver {
private:
	struct CityInfo {
		int Id, Group;

		CityInfo(int id, int group) {
			Id = id;
			Group = group;
		}

		CityInfo(CityInfo* city) {
			Id = city->Id;
			Group = city->Group;
		}
	};

	enum EvenOrOdd {
		Even = 0,	
		Odd = 1
	};

	std::list<Edge*> stos;

public:
	ApproximateSolver();
	~ApproximateSolver();

	// Inherited via ISolver
	virtual Model Solve(Model matrix) override;
	virtual void Clear() override;
	void CalculateMst(const Model& data);
	static std::list<Edge*> CalculateMst(Model& data);
	static std::list<int> GetCitiesWithOddNumerOfEdges(const std::list<Edge*>& edges, int numberOfCities);
	std::list<Edge*> CalculatePerfectMatching(const Model& data, std::list<int> oddCities);
	static std::list<Edge*> CreateGraphFromMstAndPerfectMatching(std::list<Edge*>& mst, std::list<Edge*>& perfectMatching);
	static std::list<int> CreateEulersCycle(const std::list<Edge*>& edges);
	static void CreateHamiltonsCycle(const std::list<int>& cities, Model& data);
	std::list<int> CreateHamiltonsCycle(const std::list<int>& cities);
};
