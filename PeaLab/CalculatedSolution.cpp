#include "stdafx.h"
#include "CalculatedSolution.h"

bool CalculatedSolution::operator==(const CalculatedSolution& b) const {
	if (FromCity == b.FromCity && ToCities->size() == b.ToCities->size()) {
		for (int i = 0; i < b.ToCities->size(); i++) {
			if (ToCities->at(i) != b.ToCities->at(i)) {
				return false;
			}
		}
		return true;
	}
	return false;
}

CalculatedSolution::CalculatedSolution() {}

CalculatedSolution::CalculatedSolution(int fromCity, int* toCities, int length) {
	FromCity = fromCity;
	ToCities = new std::vector<int>(toCities, toCities + length);
}


CalculatedSolution::~CalculatedSolution() {}
