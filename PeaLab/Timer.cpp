#include "stdafx.h"
#include "Timer.h"
#include <windows.h>
#include <stdio.h>

Timer::Timer() {}

Timer::~Timer() {}


void Timer::Start() {
	if (!QueryPerformanceFrequency(&Frequency))
		printf("QueryPerformanceFrequency failed!\n");

	QueryPerformanceCounter(&start_time);
}

void Timer::Stop() {
	QueryPerformanceCounter(&stop_time);
}

double Timer::GetElapsedTime() {
	LONGLONG diff = stop_time.QuadPart - start_time.QuadPart;
	return static_cast<double>(diff) * 1000 / static_cast<double>(Frequency.QuadPart);
}

void Timer::Reset() {}
