#pragma once
#include "Statistics.h"
#include <vector>

class IWriter {
public:
	virtual ~IWriter() {}
	virtual void Write(std::string filename, std::vector<Statistics> stats) = 0;
};
