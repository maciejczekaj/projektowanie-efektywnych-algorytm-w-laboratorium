#include "stdafx.h"
#include "BranchAndBoundSolver.h"
#include "Point.h"
#include <limits>
#include <iostream>
#include <string>

using namespace std;


BranchAndBoundSolver::BranchAndBoundSolver() {}


BranchAndBoundSolver::~BranchAndBoundSolver() {}

void BranchAndBoundSolver::Scale(Model& matrix) {
	ScaleRows(matrix);
	ScaleColumns(matrix);
}

void BranchAndBoundSolver::ScaleColumns(Model& matrix) {
	for (int xit = 0; xit < matrix.Columns->size(); xit++) {
		int x = matrix.Columns->at(xit);
		unsigned
				minVal = numeric_limits<int>::max();

		//find
		for (int yit = 0; yit < matrix.Rows->size(); yit++) {
			int y = matrix.Rows->at(yit);
			if (matrix.Matrix[y][x] < minVal && matrix.Matrix[y][x] > self) {
				minVal = matrix.Matrix[y][x];
			}
		}

		IncreaseLowerLimit(minVal);

		//decrease
		for (int yit = 0; yit < matrix.Rows->size(); yit++) {
			int y = matrix.Rows->at(yit);
			if (matrix.Matrix[y][x] != self)
				matrix.Matrix[y][x] -= minVal;
		}
	}
}

void BranchAndBoundSolver::ScaleRows(Model& matrix) {
	for (int xit = 0; xit < matrix.Rows->size(); xit++) {
		int x = matrix.Rows->at(xit);
		unsigned
				minVal = numeric_limits<int>::max();

		//find
		for (int yit = 0; yit < matrix.Columns->size(); yit++) {
			int y = matrix.Columns->at(yit);
			if (matrix.Matrix[x][y] < minVal && matrix.Matrix[x][y] > self) {
				minVal = matrix.Matrix[x][y];
			}
		}

		IncreaseLowerLimit(minVal);

		//decrease
		for (int yit = 0; yit < matrix.Columns->size(); yit++) {
			int y = matrix.Columns->at(yit);
			if (matrix.Matrix[x][y] != self)
				matrix.Matrix[x][y] -= minVal;
		}
	}
}

void BranchAndBoundSolver::IncreaseLowerLimit(unsigned int value) {
	lowerLimit += value;
}

void BranchAndBoundSolver::PrintLowerLimit() const {
	cout << "Dolne ograniczenie: " << lowerLimit << endl;
}

void BranchAndBoundSolver::RemoveMostCriticalPoint(CriticalPoint point, Model& matrix) {
	matrix.Rows->erase(matrix.Rows->begin() + point.x);
	matrix.Columns->erase(matrix.Columns->begin() + point.y);
}

BranchAndBoundSolver::CriticalPoint BranchAndBoundSolver::FindMostCriticalPoint(Model& model) {
	CriticalPoint criticalPoint = {0,0,0}; // fill struct with zeros
	for (int i = 0; i < model.Rows->size(); i++) {
		for (int j = 0; j < model.Columns->size(); j++) {
			int x = model.Rows->at(i);
			int y = model.Columns->at(j);

			if (model.Matrix[x][y] != 0) continue;

			unsigned minValInRow = numeric_limits<unsigned>::max(),
					minValInColumn = numeric_limits<unsigned>::max();

			//find smallest value in the row
			for (int col = 0; col < model.Columns->size(); col++) {
				int colIndex = model.Columns->at(col);
				if (colIndex == y) continue;
				int val = model.Matrix[x][colIndex];
				if (val != -1 && val < minValInRow)
					minValInRow = val;
			}

			//find smallest value in the column
			for (int row = 0; row < model.Rows->size(); row++) {
				int rowIndex = model.Rows->at(row);
				if (rowIndex == x) continue;
				int val = model.Matrix[rowIndex][y];
				if (val != -1 && val < minValInColumn)
					minValInColumn = val;
			}

			//check if current element has higher cost than current highest
			if (criticalPoint.rank < minValInColumn + minValInRow) {
				criticalPoint.rank = minValInColumn + minValInRow;
				criticalPoint.x = i;
				criticalPoint.y = j;
			}
		}
	}
	return criticalPoint;
}

void BranchAndBoundSolver::BlockBackpaths(Model& data, Point* point) {
	data.Matrix[point->Column][point->Row] = -1;
}

Model BranchAndBoundSolver::Solve(Model originalData) {
	return Solve(originalData, true);
}

void BranchAndBoundSolver::Clear() {}

Model BranchAndBoundSolver::Solve(Model originalData, bool goRight) {
	Model* data = new Model(originalData);
	auto rightChilds = new vector<unsigned>(data->NumberOfCities);
	vector<Point*> path;
	lowerLimit = 0;

	while (path.size() < data->NumberOfCities - 2) {
		Scale(*data);

		if (data->Result < lowerLimit)
			return *data;

		CriticalPoint mostCriticalPoint = FindMostCriticalPoint(*data);
		path.push_back(new Point(data->Rows->at(mostCriticalPoint.x), data->Columns->at(mostCriticalPoint.y)));
		rightChilds->push_back(lowerLimit + mostCriticalPoint.rank);
		data->Matrix[data->Columns->at(mostCriticalPoint.y)][data->Rows->at(mostCriticalPoint.x)] = -1;
		RemoveMostCriticalPoint(mostCriticalPoint, *data);
	}
	Scale(*data);
	CriticalPoint mostCriticalPoint = FindMostCriticalPoint(*data);
	path.push_back(new Point(data->Rows->at(mostCriticalPoint.x), data->Columns->at(mostCriticalPoint.y)));
	if (lowerLimit < data->Result) {
		data->Result = lowerLimit;
		data->Path = path;
	}

	if (goRight)
		for (int i = 0; i < path.size(); i++) {
			if (rightChilds->at(i) < data->Result) {
				Model* rightChild = new Model(originalData);
				auto it = path.begin();
				advance(it, i);
				rightChild->Matrix[(*it)->Column][(*it)->Row] = -1;
				rightChild->Result = data->Result;
				Model rightModel = Solve(*rightChild, false);
				if (rightModel.Result < data->Result) {
					*data = rightModel;
				}
			}
		}

	return *data;
}

void BranchAndBoundSolver::PrintPath(Model& matrix) {}
