#include "stdafx.h"
#include "CsvWriter.h"
#include <vector>
#include <ostream>
#include <fstream>


CsvWriter::CsvWriter() {}


CsvWriter::~CsvWriter() {}

void CsvWriter::Write(std::string filename, std::vector<Statistics> stats) {
	std::ofstream file;
	file.open(filename);
	if (file.is_open()) {
		file << "Liczba miast\t�redni czas\n";
		for (auto it = stats.begin(); it != stats.end(); it++) {
			file << (*it).NumberOfCities << "\t" << (*it).AverageTime << std::endl;
		}
		file.close();
	}
}
