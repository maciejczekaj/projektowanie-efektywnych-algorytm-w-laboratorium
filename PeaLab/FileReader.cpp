#include "stdafx.h"
#include "FileReader.h"
#include <fstream>
#include <iostream>

using namespace std;

FileReader::FileReader() {}


FileReader::~FileReader() {}

Model* FileReader::Read(string filename) {
	fstream fileStream;
	auto result = new Model();
	fileStream.open(filename, fstream::in);
	if (fileStream.good()) {
		fileStream >> result->NumberOfCities;
		result->Matrix = new int*[result->NumberOfCities];
		result->Rows = new vector<unsigned>(result->NumberOfCities);
		result->Columns = new vector<unsigned>(result->NumberOfCities);
		for (auto i = 0; i < result->NumberOfCities; i++)
			result->Matrix[i] = new int[result->NumberOfCities];
		for (auto i = 0; i < result->NumberOfCities; i++) {
			for (auto j = 0; j < result->NumberOfCities; j++) {
				fileStream >> result->Matrix[i][j];
			}
			result->Rows->at(i) = i;
			result->Columns->at(i) = i;
		}

		fileStream.close();
		cout << "Plik odczytano poprawnie\n\n";
	} else {
		cout << "Blad podczas odczytu pliku lub plik nie istnieje\n\n";
	}
	return result;
}
