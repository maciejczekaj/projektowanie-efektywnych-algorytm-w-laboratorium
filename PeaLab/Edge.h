#pragma once
#include "Point.h"

class Edge {
public:
	int Point1, Point2;
	unsigned Cost;
	bool Visited;
	Edge(Edge* edge);
	Edge(int point1, int point2, unsigned cost);
	~Edge();
	void Visit();
};
