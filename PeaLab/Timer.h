#pragma once
#include <Windows.h>

class Timer {
public:
	Timer();
	~Timer();

protected:
	LARGE_INTEGER Frequency;
	LARGE_INTEGER start_time, stop_time;

public:
	void Start();
	void Stop();
	void Reset();
	double GetElapsedTime();
};
