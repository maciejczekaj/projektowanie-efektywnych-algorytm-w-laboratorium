#pragma once
#include "ISolver.h"
#include "CalculatedSolution.h"
#include <unordered_set>


class HeldKarpSolver : public ISolver {
private:
	TModel GetMinimumCostRoute(Model& data, int fromCity, int* destCities, int numerOfCitiesToVisit);
	std::vector<CalculatedSolution> Solutions;
public:
	HeldKarpSolver();
	~HeldKarpSolver();
	Model Solve(Model matrix) override;
	void Clear() override;
};
