#pragma once
#include <vector>
#include "Point.h"

class TModel {
public:
	TModel();
	~TModel();
	double totalCost;
	std::vector<Point*> Path;
};
