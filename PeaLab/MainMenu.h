#pragma once
#include "Model.h"
#include "ISolver.h"

class MainMenu {
public:
	void Draw();
	MainMenu();
	~MainMenu();
private:
	Model* data;


	void ReadAction();
	void PrintAction();
	void SolveAction(ISolver* solver);
	void TestBenchAction(ISolver* solver);
};
