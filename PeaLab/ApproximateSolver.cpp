#include "stdafx.h"
#include "ApproximateSolver.h"
#include "Edge.h"
#include <algorithm>

using namespace std;

ApproximateSolver::ApproximateSolver() {}


ApproximateSolver::~ApproximateSolver() {}

Model ApproximateSolver::Solve(Model matrix) {
	auto mstEdges = CalculateMst(matrix);
	auto odd = GetCitiesWithOddNumerOfEdges(mstEdges, matrix.NumberOfCities);
	auto perfectMatching = CalculatePerfectMatching(matrix, odd);
	auto merged = CreateGraphFromMstAndPerfectMatching(mstEdges, perfectMatching);
	auto cycle = CreateEulersCycle(merged);
	CreateHamiltonsCycle(cycle, matrix);
	return matrix;
}

void ApproximateSolver::Clear() {}

list<Edge*> ApproximateSolver::CalculateMst(Model& data) {
	list<Edge*> edges, solution;
	list<CityInfo*> cities;
	edges.clear();
	solution.clear();
	cities.clear();

	for (int i = 0; i < data.NumberOfCities; ++i) {
		cities.push_back(new CityInfo(i, i));
	}

	for (auto i = 0; i < data.NumberOfCities; ++i) {
		for (auto j = 0; j < data.NumberOfCities; ++j) {
			if (i != j)
				edges.push_back(new Edge(i, j, data.Matrix[i][j]));
		}
	}

	edges.sort([](const Edge& left, const Edge& right) -> bool {
		return left.Cost < right.Cost;
	});

	while (!edges.empty()) {
		auto edge = edges.front();

		list<CityInfo*>::iterator city1 = std::find_if(cities.begin(), cities.end(),
			[&edge](const CityInfo& currentCity) -> bool {
			return currentCity.Id == edge->Point1;
		});

		list<CityInfo*>::iterator city2 = std::find_if(cities.begin(), cities.end(),
			[&edge](const CityInfo& currentCity) -> bool {
			return currentCity.Id == edge->Point2;
		});

		if ((*city1)->Group != (*city2)->Group) {
			int oldGroup = (*city2)->Group;
			int newGroup = (*city1)->Group;
			solution.push_back(edge);
			for (CityInfo* city : cities) {
				if (city->Group == oldGroup)
					city->Group = newGroup;
			}
		}

		edges.pop_front();
	}

	return solution;
}

list<int> ApproximateSolver::GetCitiesWithOddNumerOfEdges(const list<Edge*>& edges, int numberOfCities) {
	vector<EvenOrOdd>* cities = new vector<EvenOrOdd>(numberOfCities, Even);
	list<int> solution;

	for (Edge* const edge : edges) {
		cities->at(edge->Point1) = cities->at(edge->Point1) == Even ? Odd : Even;
		cities->at(edge->Point2) = cities->at(edge->Point2) == Even ? Odd : Even;
	}

	for (int i = 0; i < cities->size(); i++) {
		if (cities->at(i) == Odd)
			solution.push_back(i);
	};

	return solution;
}

list<Edge*> ApproximateSolver::CalculatePerfectMatching(const Model& data, list<int> oddCities) {

	list<Edge*> perfectMatching;
	auto lowestCost = numeric_limits<int>::max();
	do {
		list<Edge*> newSolution;
		auto costOfThisSolution = 0;

		for (int i = 0; i < oddCities.size() / 2; ++i) {
			auto srcCity = *next(oddCities.begin(), i);
			auto tgtCity = *next(oddCities.begin(), oddCities.size() / 2 + i);
			auto cost = data.Matrix[srcCity][tgtCity];
			newSolution.push_back(new Edge(srcCity, tgtCity, cost));
			costOfThisSolution += cost;
		}

		if (costOfThisSolution < lowestCost) {
			lowestCost = costOfThisSolution;
			perfectMatching = newSolution;
		}

	} while (next_permutation(oddCities.begin(), oddCities.end()));

	return perfectMatching;
}

list<Edge*> ApproximateSolver::CreateGraphFromMstAndPerfectMatching(
	list<Edge*>& mst,
	list<Edge*>& perfectMatching) {
	list<Edge*> result;
	result.insert(result.end(), mst.begin(), mst.end());
	result.insert(result.end(), perfectMatching.begin(), perfectMatching.end());
	std::stable_sort(result.begin(), result.end(),
		[](const Edge& left, const Edge& right) -> bool { return left.Point1 < right.Point1; });
	std::stable_sort(result.begin(), result.end(),
		[](const Edge& left, const Edge& right) -> bool { return left.Point2 < right.Point2; });
	return result;
}

list<int> ApproximateSolver::CreateEulersCycle(const list<Edge*>& edges) {
	list<int> result;
	auto edgesLocal = edges;
	result.push_front(0);

	while (!edgesLocal.empty()) {
		std::list<Edge*>::iterator it;
		for_each(edgesLocal.begin(), edgesLocal.end(), 
			[&current = result.front(), &it, &edgesLocal, &result](Edge* edge) -> void {
			bool isBacklink = false;
			int tgt;

			if(current == edge->Point1) {
				tgt = edge->Point2;
				isBacklink = count_if(edgesLocal.begin(), edgesLocal.end(), [&current, &next = edge->Point2](Edge* edge) -> bool
				{
					return edge->Point1 == current && edge->Point2 == next && !edge->Visited;
				}) == 2;
			} else if(current == edge->Point2) {
				tgt = edge->Point1;
				isBacklink = count_if(edgesLocal.begin(), edgesLocal.end(), [&current, &next = edge->Point2](Edge* edge) -> bool
				{
					return edge->Point2 == current && edge->Point1 == next && !edge->Visited;
				}) == 2;
			}

			if (isBacklink) {
				result.push_front(tgt);
				edge->Visit();
			}
		});

		auto beginIt = edgesLocal.begin();
		
		beginIt = std::find_if(beginIt, edgesLocal.end(), [&recentCity = result.front()](Edge* edge) -> bool {
			return !edge->Visited && (edge->Point1 == recentCity || edge->Point2 == recentCity);
		});

		if (beginIt == edgesLocal.end())
			break;
		auto successor = (*beginIt)->Point1 == result.front() ? (*beginIt)->Point2 : (*beginIt)->Point1;

		auto count = count_if(edgesLocal.begin(), edgesLocal.end(),
			[&recentCity = result.front()](Edge* edge) {
			return !edge->Visited && (edge->Point1 == recentCity || edge->Point2 == recentCity);
		});
		if (count > 1) {
			while (true) {
				if (count_if(edgesLocal.begin(), edgesLocal.end(), [&current = result.front(), &successor](Edge* edge) -> bool {
					return !edge->Visited && (edge->Point1 == successor || edge->Point2 == successor) ;
				}) > 1) {
					result.push_front(successor);
					(*beginIt)->Visit();
					beginIt = edgesLocal.begin();
      					break;
				}
				beginIt = std::find_if(beginIt, edgesLocal.end(), [&recentCity = result.front()](Edge* edge) -> bool {
					return !edge->Visited && (edge->Point1 == recentCity || edge->Point2 == recentCity);
				});
				if (beginIt == edgesLocal.end()) break;
				successor = (*beginIt)->Point1 == result.front() ? (*beginIt)->Point2 : (*beginIt)->Point1;
				beginIt++;
			}
		}
		else {
			result.push_front(successor);
			(*beginIt)->Visit();
			beginIt = edgesLocal.begin();
		}

	}

	//printf("\nEuler's Cycle\n");
	//for_each(result.begin(), result.end(), [](int& city) -> void {
	//	printf("%i\t", city);
	//});

	return result;
}


void ApproximateSolver::CreateHamiltonsCycle(const list<int>& cities, Model& data) {
	list<int> result;

	for_each(cities.begin(), cities.end(), [&result](const int& cityId)
	{
		if (none_of(result.begin(), result.end(), [&cityId](const int& currentCityId) -> bool
		{ return cityId == currentCityId; })) {
			result.push_back(cityId);
		}
	});

	data.Result = 0;
	for (int index = 0; index < result.size() - 1; index++) {
		data.Result += data.Matrix[*next(result.begin(), index)][*next(result.begin(), index + 1)];
		data.Path.push_back(new Point(*next(result.begin(), index), *next(result.begin(), index + 1)));
	}
	data.Result += data.Matrix[*next(result.begin(), result.size()-1)][*result.begin()];
	data.Path.push_back(new Point(*next(result.begin(), result.size()-1), *result.begin()));
}