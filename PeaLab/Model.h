#pragma once
#include "Point.h"
#include <vector>

class Model {
public:
	unsigned NumberOfCities, Result = UINT32_MAX;
	std::vector<unsigned> *Rows, *Columns;
	std::vector<Point*> Path;
	int** Matrix;
	Model();
	Model(const Model& copy);
	~Model();
	void ToString();
	int Find(unsigned value, bool place);
	void PrintPath();
};
