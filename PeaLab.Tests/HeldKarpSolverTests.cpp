#include "stdafx.h"
#include "HeldKarpSolverTests.h"
#include <gtest/gtest.h>
#include "Model.h"
#include "HeldKarpSolver.h"
#include "FileReader.h"


HeldKarpSolverTests::HeldKarpSolverTests()
{
}


HeldKarpSolverTests::~HeldKarpSolverTests()
{
}

TEST(HeldKarpSolver, 04Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp4.txt");
	ISolver* solver = new HeldKarpSolver();
	Model result = solver->Solve(*data);
	ASSERT_EQ(200, result.Result);
}

TEST(HeldKarpSolver, 06_1Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp6_1.txt");
	ISolver* solver = new HeldKarpSolver();
	Model result = solver->Solve(*data);
	ASSERT_EQ(132, result.Result);
}

TEST(HeldKarpSolver, 06_2Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp6_2.txt");
	ISolver* solver = new HeldKarpSolver();
	Model result = solver->Solve(*data);
	ASSERT_EQ(80, result.Result);
}

TEST(HeldKarpSolver, 10Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp10.txt");
	ISolver* solver = new HeldKarpSolver();
	Model result = solver->Solve(*data);
	ASSERT_EQ(212, result.Result);
}

TEST(HeldKarpSolver, 12Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp12.txt");
	ISolver* solver = new HeldKarpSolver();
	Model result = solver->Solve(*data);
	ASSERT_EQ(264, result.Result);
}

TEST(HeldKarpSolver, 13Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp13.txt");
	ISolver* solver = new HeldKarpSolver();
	Model result = solver->Solve(*data);
	ASSERT_EQ(269, result.Result);
}

TEST(HeldKarpSolver, 14Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp14.txt");
	ISolver* solver = new HeldKarpSolver();
	Model result = solver->Solve(*data);
	ASSERT_EQ(282, result.Result);
}

TEST(HeldKarpSolver, 15Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp15.txt");
	ISolver* solver = new HeldKarpSolver();
	Model result = solver->Solve(*data);
	ASSERT_EQ(291, result.Result);
}