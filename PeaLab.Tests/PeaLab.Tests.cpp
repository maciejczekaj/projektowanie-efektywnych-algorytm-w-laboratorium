// PeaLab.Tests.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <gtest/gtest.h>
#include <Model.h>
#include <FileReader.h>
#include <ISolver.h>
#include <BranchAndBoundSolver.h>

TEST(BranchAndBoundSolver, 04Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp4.txt");
	ISolver* solver = new BranchAndBoundSolver();
	Model result = solver->Solve(*data);
	ASSERT_EQ(200, result.Result); 
}

TEST(BranchAndBoundSolver, 06_1Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp6_1.txt");
	ISolver* solver = new BranchAndBoundSolver();
	Model result = solver->Solve(*data);
	ASSERT_EQ(132, result.Result);
}

TEST(BranchAndBoundSolver, 06_2Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp6_2.txt");
	ISolver* solver = new BranchAndBoundSolver();
	Model result = solver->Solve(*data);
	ASSERT_EQ(80, result.Result);
}

TEST(BranchAndBoundSolver, 10Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp10.txt");
	ISolver* solver = new BranchAndBoundSolver();
	Model result = solver->Solve(*data);
	ASSERT_EQ(212, result.Result);
}

TEST(BranchAndBoundSolver, 12Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp12.txt");
	ISolver* solver = new BranchAndBoundSolver();
	Model result = solver->Solve(*data);
	ASSERT_EQ(264, result.Result);
}

TEST(BranchAndBoundSolver, 13Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp13.txt");
	ISolver* solver = new BranchAndBoundSolver();
	Model result = solver->Solve(*data);
	ASSERT_EQ(269, result.Result);
}

TEST(BranchAndBoundSolver, 14Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp14.txt");
	ISolver* solver = new BranchAndBoundSolver();
	Model result = solver->Solve(*data);
	ASSERT_EQ(282, result.Result);
}

TEST(BranchAndBoundSolver, 15Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp15.txt");
	ISolver* solver = new BranchAndBoundSolver();
	Model result = solver->Solve(*data);
	ASSERT_EQ(291, result.Result);
}

int main(int ac, char* av[]) {
	testing::InitGoogleTest(&ac, av);
	return RUN_ALL_TESTS();
}
