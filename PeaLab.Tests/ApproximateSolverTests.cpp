﻿#include "stdafx.h"
#include "ApproximateSolverTests.h"
#include <gtest/gtest.h>
#include "Model.h"
#include "FileReader.h"
#include "ApproximateSolver.h"
#include <functional>

bool compareFunction(Edge* expected, Edge* current) {
	return expected->Point1 == current->Point1
			&& expected->Point2 == current->Point2
			&& expected->Cost == current->Cost;
};

TEST(ApproximateSolver, 04Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp4.txt");
	ISolver* solver = new ApproximateSolver();
	Model result = solver->Solve(*data);
	ASSERT_NEAR(250, result.Result, 50); //200-300
}

TEST(ApproximateSolver, 06_1Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp6_1.txt");
	ISolver* solver = new ApproximateSolver();
	Model result = solver->Solve(*data);
	ASSERT_NEAR(166, result.Result, 33); //132-198
}

TEST(ApproximateSolver, 06_2Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp6_2.txt");
	ISolver* solver = new ApproximateSolver();
	Model result = solver->Solve(*data);
	ASSERT_NEAR(100, result.Result, 20); //80-120
}

TEST(ApproximateSolver, 10Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp10.txt");
	ISolver* solver = new ApproximateSolver();
	Model result = solver->Solve(*data);
	ASSERT_NEAR(265, result.Result, 53); //212-318
}

TEST(ApproximateSolver, 12Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp12.txt");
	ISolver* solver = new ApproximateSolver();
	Model result = solver->Solve(*data);
	ASSERT_NEAR(330, result.Result, 66); //264-396
}

TEST(ApproximateSolver, 13Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp13.txt");
	ISolver* solver = new ApproximateSolver();
	Model result = solver->Solve(*data);
	ASSERT_NEAR(336, result.Result, 67); //269-403
}

TEST(ApproximateSolver, 14Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp14.txt");
	ISolver* solver = new ApproximateSolver();
	Model result = solver->Solve(*data);
	ASSERT_NEAR(353, result.Result, 71); //282-424
}

TEST(ApproximateSolver, 15Cities) {
	FileReader reader;
	Model* data = reader.Read("tsp//tsp15.txt");
	ISolver* solver = new ApproximateSolver();
	Model result = solver->Solve(*data);
	ASSERT_NEAR(364, result.Result, 73); //291-437
}

TEST(ApproximateSolver_MST, 04Cities) {
	std::list<Edge*> expected = {
		new Edge(0, 3, 17),
		new Edge(1, 2, 24),
		new Edge(2, 3, 67)
	};

	FileReader reader;
	Model* data = reader.Read("tsp//tsp4.txt");
	ApproximateSolver* solver = new ApproximateSolver();
	std::list<Edge*> result = solver->CalculateMst(*data);
	bool isResultValid = equal(expected.begin(), expected.end(), result.begin(), result.end(), compareFunction);
	ASSERT_TRUE(isResultValid);
}

TEST(ApproximateSolver_MST, 06_1Cities) {
	std::list<Edge*> expected = {
		new Edge(1, 2, 10),
		new Edge(2, 3, 10),
		new Edge(3, 4, 14),
		new Edge(0, 1, 20),
		new Edge(4, 5, 28)
	};

	FileReader reader;
	Model* data = reader.Read("tsp//tsp6_1.txt");
	ApproximateSolver* solver = new ApproximateSolver();
	std::list<Edge*> result = solver->CalculateMst(*data);
	bool isResultValid = equal(expected.begin(), expected.end(), result.begin(), result.end(), compareFunction);
	ASSERT_TRUE(isResultValid);
}

TEST(ApproximateSolver_MST, 06_2Cities) {
	std::list<Edge*> expected = {
		new Edge(0, 5, 4),
		new Edge(1, 3, 4),
		new Edge(4, 3, 4),
		new Edge(5, 1, 4),
		new Edge(1, 2, 10)
	};

	FileReader reader;
	Model* data = reader.Read("tsp//tsp6_2.txt");
	ApproximateSolver* solver = new ApproximateSolver();
	std::list<Edge*> result = solver->CalculateMst(*data);
	bool isResultValid = equal(expected.begin(), expected.end(), result.begin(), result.end(), compareFunction);
	ASSERT_TRUE(isResultValid);
}

TEST(ApproximateSolver_MST, 10Cities) {
	std::list<Edge*> expected = {
		new Edge(2, 4, 14),
		new Edge(1, 9, 15),
		new Edge(6, 7, 15),
		new Edge(6, 9, 16),
		new Edge(7, 8, 17),
		new Edge(0, 3, 18),
		new Edge(3, 4, 19),
		new Edge(1, 3, 21),
		new Edge(5, 7, 21)
	};

	FileReader reader;
	Model* data = reader.Read("tsp//tsp10.txt");
	ApproximateSolver* solver = new ApproximateSolver();
	std::list<Edge*> result = solver->CalculateMst(*data);
	bool isResultValid = equal(expected.begin(), expected.end(), result.begin(), result.end(), compareFunction);
	ASSERT_TRUE(isResultValid);
}

TEST(ApproximateSolver_MST, 12Cities
) {
	std::list<Edge*> expected = {
		new Edge(3, 5, 15),
		new Edge(7, 9, 15),
		new Edge(2, 11, 21),
		new Edge(3, 10, 21),
		new Edge(4, 8, 21),
		new Edge(9, 11, 21),
		new Edge(1, 7, 23),
		new Edge(1, 8, 23),
		new Edge(2, 6, 23),
		new Edge(5, 7, 23),
		new Edge(0, 1, 29)
	};

	FileReader reader;
	Model* data = reader.Read("tsp//tsp12.txt");
	ApproximateSolver* solver = new ApproximateSolver();
	std::list<Edge*> result = solver->CalculateMst(*data);
	bool isResultValid = equal(expected.begin(), expected.end(), result.begin(), result.end(), compareFunction);
	ASSERT_TRUE(isResultValid);
}

TEST(ApproximateSolver_MST, 13Cities) {
	std::list<Edge*> expected = {
		new Edge(1, 12, 11),
		new Edge(3, 5, 15),
		new Edge(7, 9, 15),
		new Edge(2, 11, 21),
		new Edge(3, 10, 21),
		new Edge(4, 8, 21),
		new Edge(9, 11, 21),
		new Edge(0, 12, 23),
		new Edge(1, 7, 23),
		new Edge(1, 8, 23),
		new Edge(2, 6, 23),
		new Edge(5, 7, 23)
	};

	FileReader reader;
	Model* data = reader.Read("tsp//tsp13.txt");
	ApproximateSolver* solver = new ApproximateSolver();
	std::list<Edge*> result = solver->CalculateMst(*data);
	bool isResultValid = equal(expected.begin(), expected.end(), result.begin(), result.end(), compareFunction);
	ASSERT_TRUE(isResultValid);
}

TEST(ApproximateSolver_MST, 14Cities) {
	std::list<Edge*> expected = {
		new Edge(1, 12, 11),
		new Edge(11, 13, 11),
		new Edge(3, 5, 15),
		new Edge(7, 9, 15),
		new Edge(2, 11, 21),
		new Edge(3, 10, 21),
		new Edge(4, 8, 21),
		new Edge(9, 11, 21),
		new Edge(0, 12, 23),
		new Edge(1, 7, 23),
		new Edge(1, 8, 23),
		new Edge(2, 6, 23),
		new Edge(5, 7, 23)
	};

	FileReader reader;
	Model* data = reader.Read("tsp//tsp14.txt");
	ApproximateSolver* solver = new ApproximateSolver();
	std::list<Edge*> result = solver->CalculateMst(*data);
	bool isResultValid = equal(expected.begin(), expected.end(), result.begin(), result.end(), compareFunction);
	ASSERT_TRUE(isResultValid);
}

TEST(ApproximateSolver_MST, 15Cities) {
	std::list<Edge*> expected = {
		new Edge(1, 12, 11),
		new Edge(8, 14, 11),
		new Edge(11, 13, 11),
		new Edge(3, 5, 15),
		new Edge(7, 9, 15),
		new Edge(1, 14, 21),
		new Edge(2, 11, 21),
		new Edge(3, 10, 21),
		new Edge(4, 8, 21),
		new Edge(9, 11, 21),
		new Edge(0, 12, 23),
		new Edge(1, 7, 23),
		new Edge(2, 6, 23),
		new Edge(5, 7, 23)
	};

	FileReader reader;
	Model* data = reader.Read("tsp//tsp15.txt");
	ApproximateSolver* solver = new ApproximateSolver();
	std::list<Edge*> result = solver->CalculateMst(*data);
	bool isResultValid = equal(expected.begin(), expected.end(), result.begin(), result.end(), compareFunction);
	ASSERT_TRUE(isResultValid);
}
